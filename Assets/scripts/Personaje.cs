﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour {
    
	public float velocidadDespzamiento = 0.5f;
	public float turnSpeed = 200f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// Mover a delante
		if (Input.GetKey(KeyCode.UpArrow)) {
			Debug.Log("up");
			this.transform.Translate(Vector3.forward * velocidadDespzamiento);
		}
	    // Mover atras
		if (Input.GetKey(KeyCode.DownArrow)) {
			Debug.Log("down");
			this.transform.Translate(Vector3.back * velocidadDespzamiento);
		}
		// Girar izquierda
		if (Input.GetKey(KeyCode.LeftArrow)) {
			Debug.Log("left");
			transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime);
		}
		// Girar derecha
		if (Input.GetKey(KeyCode.RightArrow)) {
			Debug.Log("right");
			transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime);
		}
	
	}
}
